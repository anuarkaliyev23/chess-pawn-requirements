package kz.mathncode.anuarkaliyev23.example.chess.ui;

import kz.mathncode.anuarkaliyev23.example.chess.game.Move;

public interface UI {
    void showDesk();
    Move nextMove();
}
