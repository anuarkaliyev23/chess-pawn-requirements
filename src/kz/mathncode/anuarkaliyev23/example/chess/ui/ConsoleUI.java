package kz.mathncode.anuarkaliyev23.example.chess.ui;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ChessException;
import kz.mathncode.anuarkaliyev23.example.chess.game.ChessBoard;
import kz.mathncode.anuarkaliyev23.example.chess.game.Move;
import kz.mathncode.anuarkaliyev23.example.chess.game.PawnTransformationMove;
import kz.mathncode.anuarkaliyev23.example.chess.game.SimpleMove;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.PieceType;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

import java.util.Locale;
import java.util.Scanner;

public class ConsoleUI implements UI{
    private final ChessBoard chessBoard;
    private final Scanner scanner;

    public ConsoleUI(ChessBoard chessBoard, Scanner scanner) {
        this.chessBoard = chessBoard;
        this.scanner = scanner;
    }

    public ChessBoard getChessBoard() {
        return chessBoard;
    }

    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public void showDesk() {
        for (Horizontal h : Horizontal.values()) {
            for (Vertical v : Vertical.values()) {
                BoardPosition position = new BoardPosition(v, h);
                Piece piece = chessBoard.pieceByPosition(position);
                System.out.print(VERTICAL_LINE);
                if (piece == null) {
                    System.out.print(EMPTY_FIELD);
                } else {
                    System.out.print(piece.symbol());
                }
                System.out.print(VERTICAL_LINE);
            }
            System.out.println();
        }
    }

    @Override
    public Move nextMove() {
        String line = scanner.nextLine();
        String[] splits = line.split(" ");
        if (splits.length == 3) {
            return simpleMoveFromSplit(splits);
        } else if (splits.length == 4) {
            return pawnTransformationMoveFromSplit(splits);
        } else {
            throw new ChessException("Invalid Move Format!");
        }
    }

    private SimpleMove simpleMoveFromSplit(String[] splits) {
        String first = splits[0];
        String second = splits[2];
        BoardPosition start = positionFromAlgebraicNotation(first);
        BoardPosition finish = positionFromAlgebraicNotation(second);
        return new SimpleMove(start, finish);
    }

    private PawnTransformationMove pawnTransformationMoveFromSplit(String[] splits) {
        String first = splits[0];
        String second = splits[2];
        String pieceTypeString = splits[3];
        BoardPosition start = positionFromAlgebraicNotation(first);
        BoardPosition finish = positionFromAlgebraicNotation(second);
        PieceType pieceType = PieceType.valueOf(pieceTypeString);
        return new PawnTransformationMove(start, finish, pieceType);
    }

    private BoardPosition positionFromAlgebraicNotation(String notation) {
        String verticalString = String.valueOf(notation.charAt(0)).toUpperCase();
        int horizontalIndex = Integer.valueOf(String.valueOf(notation.charAt(1)));
        Vertical vertical = Vertical.valueOf(verticalString);
        Horizontal horizontal = Horizontal.fromIndex(horizontalIndex);
        return new BoardPosition(vertical, horizontal);
    }

    private static final String EMPTY_FIELD = "\uff3f";
    private static final String VERTICAL_LINE = "\uff5c";

}
