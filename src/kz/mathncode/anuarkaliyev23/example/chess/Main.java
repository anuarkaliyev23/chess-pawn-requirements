package kz.mathncode.anuarkaliyev23.example.chess;

import kz.mathncode.anuarkaliyev23.example.chess.game.ChessBoard;
import kz.mathncode.anuarkaliyev23.example.chess.game.Move;
import kz.mathncode.anuarkaliyev23.example.chess.game.SimpleMove;
import kz.mathncode.anuarkaliyev23.example.chess.ui.ConsoleUI;
import kz.mathncode.anuarkaliyev23.example.chess.ui.UI;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ChessBoard chessBoard = new ChessBoard();
        UI ui = new ConsoleUI(chessBoard, new Scanner(System.in));
        while (!chessBoard.isFinishedGame()) {
            ui.showDesk();
            Move move = ui.nextMove();
            chessBoard.handle(move);
        }
    }
}
