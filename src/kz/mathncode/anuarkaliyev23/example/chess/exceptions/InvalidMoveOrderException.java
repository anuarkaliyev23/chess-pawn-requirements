package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;

public class InvalidMoveOrderException extends ChessException {
    private final Piece piece;

    public InvalidMoveOrderException(Piece piece) {
        super(String.format("piece {%s} can't move in other side's turn", piece));
        this.piece = piece;
    }

    public Piece getPiece() {
        return piece;
    }
}
