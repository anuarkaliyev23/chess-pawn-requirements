package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PawnTest {

    @Test
    public void checkMoves() {
        Pawn pawn = new Pawn(new BoardPosition(Vertical.E, Horizontal.II), true, false);
        assertTrue(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.III)));
        assertTrue(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.IV)));

        assertFalse(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.V)));
        assertFalse(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.VI)));
        assertFalse(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.VII)));
        assertFalse(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.VIII)));
        assertFalse(pawn.possibleMove(new BoardPosition(Vertical.E, Horizontal.I)));
    }

    @Test
    public void checkTrace() {
        Pawn pawn = new Pawn(new BoardPosition(Vertical.E, Horizontal.II), true, false);
        Set<BoardPosition> trace = pawn.trace(new BoardPosition(Vertical.E, Horizontal.IV));
        assertTrue(trace.contains(new BoardPosition(Vertical.E, Horizontal.III)));
    }

    @Test
    public void checkEat() {
        Pawn pawn = new Pawn(new BoardPosition(Vertical.E, Horizontal.II), true, false);
        assertTrue(pawn.possibleEat(new BoardPosition(Vertical.F, Horizontal.III)));
        assertTrue(pawn.possibleEat(new BoardPosition(Vertical.D, Horizontal.III)));
        assertFalse(pawn.possibleEat(new BoardPosition(Vertical.F, Horizontal.I)));
        assertFalse(pawn.possibleEat(new BoardPosition(Vertical.D, Horizontal.I)));

    }
}
