package kz.mathncode.anuarkaliyev23.example.chess.ui;

import kz.mathncode.anuarkaliyev23.example.chess.game.ChessBoard;
import kz.mathncode.anuarkaliyev23.example.chess.game.ChessBoardFactory;
import kz.mathncode.anuarkaliyev23.example.chess.game.ClassicChessBoard;
import kz.mathncode.anuarkaliyev23.example.chess.game.ClassicChessBoardProxy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessBoardConsoleUITest {

    @Test
    void showBoard() {
        ChessBoardFactory factory = new ChessBoardFactory();
        ChessBoard board = factory.produce();
        ChessBoardUI ui = new ChessBoardConsoleUI(board);

        ui.showBoard();
    }
}