package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.Knight;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest  {

    @Test
    public void possibleMove() {
        Knight knight = new Knight(new BoardPosition(Vertical.E, Horizontal.IV), true, true);
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.G, Horizontal.V)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.G, Horizontal.III)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.F, Horizontal.VI)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.F, Horizontal.II)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.D, Horizontal.II)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.D, Horizontal.VI)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.C, Horizontal.V)));
        assertTrue(knight.possibleMove(new BoardPosition(Vertical.C, Horizontal.III)));

        assertFalse(knight.possibleMove(new BoardPosition(Vertical.A, Horizontal.I)));
    }
}
