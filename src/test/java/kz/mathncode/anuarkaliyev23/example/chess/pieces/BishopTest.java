package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BishopTest {

    @Test
    public void checkMoves() {
        Bishop bishop = new Bishop(new BoardPosition(Vertical.E, Horizontal.IV), true, true);

        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.F, Horizontal.V)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.G, Horizontal.VI)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.H, Horizontal.VII)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.D, Horizontal.III)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.C, Horizontal.II)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.B, Horizontal.I)));

        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.F, Horizontal.III)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.G, Horizontal.II)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.H, Horizontal.I)));

        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.D, Horizontal.V)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.C, Horizontal.VI)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.B, Horizontal.VII)));
        assertTrue(bishop.possibleMove(new BoardPosition(Vertical.A, Horizontal.VIII)));

        assertFalse(bishop.possibleMove(new BoardPosition(Vertical.F, Horizontal.IV)));
        assertFalse(bishop.possibleMove(new BoardPosition(Vertical.D, Horizontal.IV)));
    }

    @Test
    public void checkTrace() {
        Bishop bishop = new Bishop(new BoardPosition(Vertical.E, Horizontal.IV), true, true);
        Set<BoardPosition> trace = bishop.trace(new BoardPosition(Vertical.A, Horizontal.VIII));
        assertTrue(trace.contains(new BoardPosition(Vertical.D, Horizontal.V)));
        assertTrue(trace.contains(new BoardPosition(Vertical.C, Horizontal.VI)));
        assertTrue(trace.contains(new BoardPosition(Vertical.B, Horizontal.VII)));

        Set<BoardPosition> trace1 = bishop.trace(new BoardPosition(Vertical.H, Horizontal.I));
        System.out.println(trace1);
    }
}