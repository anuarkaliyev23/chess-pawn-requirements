package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.*;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChessBoardFactoryTest {

    @Test
    void produce() {
        ChessBoardFactory factory = new ChessBoardFactory();
        ChessBoard board = factory.produce();

        assertEquals(new Pawn(new BoardPosition(Vertical.A, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.A, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.B, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.B, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.C, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.C, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.D, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.D, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.E, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.E, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.F, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.F, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.G, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.G, Horizontal.II)));
        assertEquals(new Pawn(new BoardPosition(Vertical.H, Horizontal.II), true, false), board.pieceByPosition(new BoardPosition(Vertical.H, Horizontal.II)));

        assertEquals(new Pawn(new BoardPosition(Vertical.A, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.A, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.B, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.B, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.C, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.C, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.D, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.D, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.E, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.E, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.F, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.F, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.G, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.G, Horizontal.VII)));
        assertEquals(new Pawn(new BoardPosition(Vertical.H, Horizontal.VII), false, false), board.pieceByPosition(new BoardPosition(Vertical.H, Horizontal.VII)));


        assertEquals(new Rook(new BoardPosition(Vertical.A, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.A, Horizontal.I)));
        assertEquals(new Knight(new BoardPosition(Vertical.B, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.B, Horizontal.I)));
        assertEquals(new Bishop(new BoardPosition(Vertical.C, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.C, Horizontal.I)));
        assertEquals(new Queen(new BoardPosition(Vertical.D, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.D, Horizontal.I)));
        assertEquals(new King(new BoardPosition(Vertical.E, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.E, Horizontal.I)));
        assertEquals(new Bishop(new BoardPosition(Vertical.F, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.F, Horizontal.I)));
        assertEquals(new Knight(new BoardPosition(Vertical.G, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.G, Horizontal.I)));
        assertEquals(new Rook(new BoardPosition(Vertical.H, Horizontal.I), true, false), board.pieceByPosition(new BoardPosition(Vertical.H, Horizontal.I)));

        assertEquals(new Rook(new BoardPosition(Vertical.A, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.A, Horizontal.VIII)));
        assertEquals(new Knight(new BoardPosition(Vertical.B, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.B, Horizontal.VIII)));
        assertEquals(new Bishop(new BoardPosition(Vertical.C, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.C, Horizontal.VIII)));
        assertEquals(new Queen(new BoardPosition(Vertical.D, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.D, Horizontal.VIII)));
        assertEquals(new King(new BoardPosition(Vertical.E, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.E, Horizontal.VIII)));
        assertEquals(new Bishop(new BoardPosition(Vertical.F, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.F, Horizontal.VIII)));
        assertEquals(new Knight(new BoardPosition(Vertical.G, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.G, Horizontal.VIII)));
        assertEquals(new Rook(new BoardPosition(Vertical.H, Horizontal.VIII), false, false), board.pieceByPosition(new BoardPosition(Vertical.H, Horizontal.VIII)));

        assertEquals(32, board.pieces().size());
        System.out.println(board);
    }
}