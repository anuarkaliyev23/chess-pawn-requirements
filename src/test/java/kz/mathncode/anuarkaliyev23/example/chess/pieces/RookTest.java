package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.Rook;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Класс для проведения тестов на валидность хода ладьи
 * */
public class RookTest {

    @Test
    public void checkMoves() {
        Rook rook = new Rook(new BoardPosition(Vertical.A, Horizontal.I), true, false);
        assertTrue (rook.possibleMove(new BoardPosition(Vertical.A, Horizontal.VIII)));
        assertTrue(rook.possibleMove(new BoardPosition(Vertical.G, Horizontal.I)));
        assertFalse(rook.possibleMove(new BoardPosition(Vertical.B, Horizontal.II)));
    }

    @Test
    public void checkTrace() {
        Rook rook = new Rook(new BoardPosition(Vertical.A, Horizontal.I), true, false);
        Set<BoardPosition> trace = rook.trace(new BoardPosition(Vertical.A, Horizontal.VIII));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.II)));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.III)));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.IV)));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.V)));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.VI)));
        assertTrue(trace.contains(new BoardPosition(Vertical.A, Horizontal.VII)));
    }
}
