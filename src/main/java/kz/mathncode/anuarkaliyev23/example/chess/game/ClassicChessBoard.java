package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.InvalidBoardException;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.NotImplementedException;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.PieceNotFoundException;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.*;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс шахматной доски
 * */
public class ClassicChessBoard implements ChessBoard {
    /**
     * Список фигур
     * */
    private List<Piece> pieces;
    /**
     * Флаг того, чей ход
     * */
    private boolean isWhiteMove;

    public ClassicChessBoard(ClassicChessBoard other) {
        List<Piece> otherPieces = other.getPieces();
        for (Piece piece : pieces) {
            PieceFactory factory = new PieceFactory(piece.getClass(), piece.getBoardPosition(), piece.isWhite(), piece.isMoved());
            otherPieces.add(factory.produce());
        }
        this.pieces = otherPieces;
        this.isWhiteMove = other.isWhiteMove();

        if (!hasKings())
            throw new InvalidBoardException("Can't have boards without 2 Kings of Different colors");
    }

    public ClassicChessBoard(List<Piece> pieces, boolean isWhiteMove) {
        this.pieces = pieces;
        this.isWhiteMove = isWhiteMove;

        if (!hasKings())
            throw new InvalidBoardException("Can't have boards without 2 Kings of Different colors");
    }


    public List<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public boolean isWhiteMove() {
        return isWhiteMove;
    }

    public void setWhiteMove(boolean whiteMove) {
        isWhiteMove = whiteMove;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassicChessBoard that = (ClassicChessBoard) o;
        return isWhiteMove == that.isWhiteMove && Objects.equals(pieces, that.pieces);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pieces, isWhiteMove);
    }

    @Override
    public String toString() {
        return "ChessBoard{" +
                "pieces=" + pieces +
                ", isWhiteMove=" + isWhiteMove +
                '}';
    }

    /**
     * Метод призванный проводить операции хода
     * */
    @Override
    public void handle(Move move) {
        if (move instanceof SimpleMove) {
            handleSimpleMove((SimpleMove) move);
        } else if (move instanceof PawnTransformationMove) {
            handlePawnTransformationMove((PawnTransformationMove) move);
        } else
            throw new NotImplementedException();
        isWhiteMove = !isWhiteMove;
    }

    @Override
    public List<Piece> pieces() {
        return pieces;
    }

    @Override
    public ChessBoardStatus status() {
        if (isCheck(true)) {
            return ChessBoardStatus.WHITE_CHECK;
        } else if (isCheck(false)) {
            return ChessBoardStatus.BLACK_CHECK;
        } else
        return ChessBoardStatus.ORDINARY;
    }

    @Override
    public List<Move> possibleMoves(Piece p) {
        List<Move> result = new ArrayList<>();
        for (Vertical vertical : Vertical.values()) {
            for (Horizontal horizontal : Horizontal.values()) {
                BoardPosition position = new BoardPosition(vertical, horizontal);
                Move move = new SimpleMove(p.getBoardPosition(), position);
                if ((p.possibleMove(position) || p.possibleEat(position)) && emptyTrace(p.trace(position)))
                    result.add(move);
            }
        }
        return result;
    }

    /**
     * Метод призванный проводить операции простого хода
     *
     * @param move - ход, который нужно провести
     * */
    private void handleSimpleMove(SimpleMove move) {
        Piece piece = pieceByPosition(move.getStart());

        if (!isValidMove(move))
            throw new ImpossibleMoveException(piece, move.getTarget());

        moveOrEat(move);
    }

    private void moveOrEat(Move move) {
        Piece piece = pieceByPosition(move.getStart());

        Piece targetPiece = pieceByPosition(move.getTarget());
        if (piece == null) {
            throw new PieceNotFoundException(move.getStart());
        }

        if (targetPiece instanceof King) {
            throw new ImpossibleMoveException(piece, move.getTarget());
        }

        if (targetPiece != null) {
            if (piece.possibleEat(targetPiece.getBoardPosition())) {
                movePiece(piece, move.getTarget());
                pieces.remove(targetPiece);
            } else {
                throw new ImpossibleMoveException(piece, move.getTarget());
            }
        } else {
            movePiece(piece, move.getTarget());
        }
    }

    private boolean hasKings() {
        List<Piece> kings = pieces.stream()
                .filter((Piece it) -> it instanceof King)
                .collect(Collectors.toList());

        return (kings.size() == KING_QUANTITY && kings.get(0).isWhite() != kings.get(1).isWhite());
    }

    private void handlePawnTransformationMove(PawnTransformationMove move) {
        Piece piece = pieceByPosition(move.getStart());

        if (!isValidMove(move))
            throw new ImpossibleMoveException(piece, move.getTarget());

        moveOrEat(move);
        PieceFactory factory = new PieceFactory(move.getTransformTo().getPieceClass(), move.getTarget(), piece.isWhite(), false);
        Piece transform = factory.produce();
        pieces.add(transform);
    }


    private boolean isValidMove(Move move) {
        if (move instanceof SimpleMove) {
            return isValidSimpleMove((SimpleMove) move);
        } else if (move instanceof PawnTransformationMove) {
            return isValidPawnTransformationMove((PawnTransformationMove) move);
        } else {
            throw new NotImplementedException();
        }
    }

    private boolean isValidMovingPiece(Piece piece) {
        return piece.isWhite() == isWhiteMove;
    }

    private boolean isValidSimpleMove(SimpleMove move) {
        Piece piece = pieceByPosition(move.getStart());
        if (piece == null)
            return false;

        if (!isValidMovingPiece(piece))
            throw new ImpossibleMoveException(piece, move.getTarget());

        if (!piece.possibleMove(move.getTarget()) && !piece.possibleEat(move.getTarget()))
            return false;

        Set<BoardPosition> trace = piece.trace(move.getTarget());

        if (!emptyTrace(trace))
            return false;

        Piece targetPiece = pieceByPosition(move.getTarget());
        if (targetPiece != null) {
            return targetPiece.isWhite() != piece.isWhite();
        }
        return true;
    }

    private boolean isValidPawnTransformationMove(PawnTransformationMove move) {
        if (!isValidSimpleMove(new SimpleMove(move.getStart(), move.getTarget())))
            return false;

        Piece piece = pieceByPosition(move.getStart());

        return  (piece.isWhite() && piece.getBoardPosition().getHorizontal() == Horizontal.VII)
                || (!piece.isWhite() && piece.getBoardPosition().getHorizontal() == Horizontal.II);

    }

    /**
     * Метод, который фактически двигает фигуру
     * */
    private void movePiece(Piece piece, BoardPosition position) {
        piece.setBoardPosition(position);
        piece.setMoved(false);
    }

    private boolean isCheck(boolean isWhite) {
        final King king =  (King) pieces
                .stream()
                .filter((Piece it) -> it.isWhite() == isWhite && it instanceof King)
                .findFirst()
                .get();

        List<Piece> enemyPieces = pieces
                .stream()
                .filter((Piece it) -> it.isWhite() != isWhite)
                .collect(Collectors.toList());

        List<Piece> attacking = enemyPieces
                .stream()
                .filter((Piece it) -> emptyTrace(it.trace(king.getBoardPosition())))
                .collect(Collectors.toList());

        return !attacking.isEmpty();
    }

    /**
     * Метод призванный для проверки наличия фигур по траектории движения
     *
     * @param trace - множество всех позиций, составляющие траекторию фигуры
     * @return true - если фигур нет на пути и false если есть
     * */
    private boolean emptyTrace(Set<BoardPosition> trace) {
        for (BoardPosition position : trace) {
            Piece piece = pieceByPosition(position);
            if (piece != null)
                return false;
        }
        return true;
    }

    /**
     * Метод для нахождения фигуры по ее позиции
     *
     * @param position - позиция искомой фигуры
     * @return фигуру или null, если таковой фигуры не найдено
     * */
    @Override
    public Piece pieceByPosition(BoardPosition position) {
        for (Piece p : pieces) {
            if (p.getBoardPosition().equals(position))
                return p;
        }
        return null;
    }

    private static final int KING_QUANTITY = 2;
}
