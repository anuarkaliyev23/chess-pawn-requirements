package kz.mathncode.anuarkaliyev23.example.chess.position;

/**
 * Перечисления для хранения вертикалей
 * */
public enum Vertical {
    A(1),
    B(2),
    C(3),
    D(4),
    E(5),
    F(6),
    G(7),
    H(8);

    /**
     * Индекс, хранящий порядковый номер
     * */
    private final int index;

    Vertical(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public Vertical next() {
        if (this == Vertical.H) {
            return A;
        } else {
            return Vertical.fromIndex(getIndex() + 1);
        }
    }

    public Vertical prev() {
        if (this == Vertical.A) {
            return H;
        } else {
            return Vertical.fromIndex(getIndex() - 1);
        }
    }

    public static Vertical fromIndex(int index) {
        return Vertical.values()[index - 1];
    }
}
