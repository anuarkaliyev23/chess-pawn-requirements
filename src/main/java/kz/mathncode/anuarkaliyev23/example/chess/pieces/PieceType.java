package kz.mathncode.anuarkaliyev23.example.chess.pieces;

/**
 * Перечисление, содержащее все виды фигур
 * */
public enum PieceType {
    PAWN("P", Pawn.class),
    ROOK("R", Rook.class),
    KNIGHT("N", Knight.class),
    BISHOP("B", Bishop.class),
    QUEEN("Q", Queen.class),
    KING("K", King.class);

    private final String symbol;
    private final Class<? extends Piece> pieceClass;

    PieceType(String symbol, Class<? extends Piece> pieceClass) {
        this.symbol = symbol;
        this.pieceClass = pieceClass;
    }

    public String getSymbol() {
        return symbol;
    }

    public Class<? extends Piece> getPieceClass() {
        return pieceClass;
    }
}
