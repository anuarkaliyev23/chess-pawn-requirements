package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.configuration.Factory;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.*;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

import java.util.ArrayList;
import java.util.List;

public class ChessBoardFactory implements Factory<ChessBoard> {
    @Override
    public ChessBoard produce() {
        return new ClassicChessBoardProxy(new ClassicChessBoard(pieces(), true));
    }

    private List<Piece> pieces() {
        List<Piece> pieces = new ArrayList<>();
        for (int i = 0; i < PAWN_QUANTITY; i++) {
            pieces.add(new Pawn(new BoardPosition(Vertical.fromIndex(i + 1), Horizontal.II), true, false));
            pieces.add(new Pawn(new BoardPosition(Vertical.fromIndex(i + 1), Horizontal.VII), false, false));
        }
        pieces.add(new Rook(new BoardPosition(Vertical.A, Horizontal.I), true, false));
        pieces.add(new Rook(new BoardPosition(Vertical.A, Horizontal.VIII), false, false));
        pieces.add(new Rook(new BoardPosition(Vertical.H, Horizontal.I), true, false));
        pieces.add(new Rook(new BoardPosition(Vertical.H, Horizontal.VIII), false, false));

        pieces.add(new Knight(new BoardPosition(Vertical.B, Horizontal.I), true, false));
        pieces.add(new Knight(new BoardPosition(Vertical.B, Horizontal.VIII), false, false));
        pieces.add(new Knight(new BoardPosition(Vertical.G, Horizontal.I), true, false));
        pieces.add(new Knight(new BoardPosition(Vertical.G, Horizontal.VIII), false, false));

        pieces.add(new Bishop(new BoardPosition(Vertical.C, Horizontal.I), true, false));
        pieces.add(new Bishop(new BoardPosition(Vertical.C, Horizontal.VIII), false, false));
        pieces.add(new Bishop(new BoardPosition(Vertical.F, Horizontal.I), true, false));
        pieces.add(new Bishop(new BoardPosition(Vertical.F, Horizontal.VIII), false, false));

        pieces.add(new Queen(new BoardPosition(Vertical.D, Horizontal.I), true, false));
        pieces.add(new Queen(new BoardPosition(Vertical.D, Horizontal.VIII), false, false));

        pieces.add(new King(new BoardPosition(Vertical.E, Horizontal.I), true, false));
        pieces.add(new King(new BoardPosition(Vertical.E, Horizontal.VIII), false, false));

        return pieces;
    }

    private static final int PAWN_QUANTITY = 8;
    private static final int ROOK_QUANTITY = 2;
    private static final int BISHOP_QUANTITY = 2;
    private static final int KNIGHT_QUANTITY = 2;
    private static final int QUEEN_QUANTITY = 1;
    private static final int KING_QUANTITY = 1;
}
