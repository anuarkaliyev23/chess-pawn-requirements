package kz.mathncode.anuarkaliyev23.example.chess.configuration;

public interface Proxy<T> {
    T instance();
}
