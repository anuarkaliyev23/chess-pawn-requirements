package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

public class InvalidBoardException extends ChessException {
    public InvalidBoardException() {
    }

    public InvalidBoardException(String message) {
        super(message);
    }

    public InvalidBoardException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBoardException(Throwable cause) {
        super(cause);
    }
}
