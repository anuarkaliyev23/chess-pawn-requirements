package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

/**
 * Простой ход
 * */
public class SimpleMove extends Move{
    public SimpleMove(BoardPosition start, BoardPosition target) {
        super(start, target);
    }
}
