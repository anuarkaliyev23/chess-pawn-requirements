package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.configuration.Factory;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.NotImplementedException;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

public class PieceFactory implements Factory<Piece> {
    private final Class<? extends Piece> pieceClass;
    private final BoardPosition boardPosition;
    private final boolean isWhite;
    private final boolean hasMoved;

    public PieceFactory(Class<? extends Piece> pieceClass, BoardPosition boardPosition, boolean isWhite, boolean hasMoved) {
        this.pieceClass = pieceClass;
        this.boardPosition = boardPosition;
        this.isWhite = isWhite;
        this.hasMoved = hasMoved;
    }

    @Override
    public Piece produce() {
        if (pieceClass == Pawn.class) {
            return new Pawn(boardPosition, isWhite, hasMoved);
        } else if (pieceClass == Rook.class) {
            return new Rook(boardPosition, isWhite, hasMoved);
        } else if (pieceClass == Knight.class) {
            return new Knight(boardPosition, isWhite, hasMoved);
        } else if (pieceClass == Bishop.class) {
            return new Bishop(boardPosition, isWhite, hasMoved);
        } else if (pieceClass == Queen.class) {
            return new Queen(boardPosition, isWhite, hasMoved);
        } else if (pieceClass == King.class) {
            return new King(boardPosition, isWhite, hasMoved);
        } else throw new NotImplementedException();
    }
}
