package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.Objects;
import java.util.Set;

/**
 * Класс обозначающий фигуру
 * */
public abstract class Piece {
    private BoardPosition boardPosition;
    private boolean isWhite;
    private boolean isMoved;

    public Piece(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        this.boardPosition = boardPosition;
        this.isWhite = isWhite;
        this.isMoved = isMoved;
    }

    public Piece(Piece other) {
        this(new BoardPosition(other.boardPosition), other.isWhite, other.isMoved);
    }

    public BoardPosition getBoardPosition() {
        return boardPosition;
    }

    public void setBoardPosition(BoardPosition boardPosition) {
        this.boardPosition = boardPosition;
    }

    public boolean isWhite() {
        return isWhite;
    }

    public void setWhite(boolean white) {
        isWhite = white;
    }

    public boolean isMoved() {
        return isMoved;
    }

    public void setMoved(boolean moved) {
        isMoved = moved;
    }


    /**
     * Метод, призванный определить способна ли фигура пойти на заданную координату.
     * Оценка не предполагает наличия других фигур на доске. То есть ход теоретический, как будто
     * на пустой доске
     *
     * @param target - поле, на возможность хода к которому производится анализ
     * @return true если фигура может пойти, false - если не может
     * */
    public abstract boolean possibleMove(BoardPosition target);

    /**
     * Метод, призванный вернуть траекторию движения фигуры до определенного поля.
     * В Set входят только те поля, которые не являются начальным или конечным положением фигуры,
     * то есть все поля между текущей и конечной координатами на пути фигуры
     *
     * @param target - поле, на возможность хода к которому производится анализ
     * @return Set всех полей, через которые фигура, в теории, пройдет на пути к цели
     * */
    public abstract Set<BoardPosition> trace(BoardPosition target);

    /**
     * Метод призванный определить, способна ли фигура "съесть" на заданную координату.
     * Оценка не предполагает наличия других фигур на доске. То есть ход теоретический, как будто
     * на пустой доске
     *
     * @param target - поле, на возможность хода к которому производится анализ
     * @return true если фигура может пойти, false - если не может
     * */
    public boolean possibleEat(BoardPosition target) {
        return this.possibleMove(target);
    }

    public abstract String unicodeSymbol();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return isWhite == piece.isWhite && isMoved == piece.isMoved && Objects.equals(boardPosition, piece.boardPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(boardPosition, isWhite, isMoved);
    }

    @Override
    public String toString() {
        return "Piece{" +
                "boardPosition=" + boardPosition +
                ", isWhite=" + isWhite +
                ", isMoved=" + isMoved +
                '}';
    }
}
