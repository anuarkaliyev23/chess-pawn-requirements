package kz.mathncode.anuarkaliyev23.example.chess.configuration;

public interface Factory<T> {
    T produce();
}
