package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.PieceType;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.Objects;

/**
 * Ход трансформации пешки
 * */
public class PawnTransformationMove extends Move {
    private PieceType transformTo;

    public PawnTransformationMove(BoardPosition start, BoardPosition target, PieceType transformTo) {
        super(start, target);
        this.transformTo = transformTo;
    }

    public PieceType getTransformTo() {
        return transformTo;
    }

    public void setTransformTo(PieceType transformTo) {
        this.transformTo = transformTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PawnTransformationMove that = (PawnTransformationMove) o;
        return transformTo == that.transformTo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), transformTo);
    }

    @Override
    public String toString() {
        return "PawnTransformationMove{" +
                "transformTo=" + transformTo +
                "} " + super.toString();
    }
}
