package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.Objects;

/**
 * Базовый класс для всех видов хода
 * */
public abstract class Move {
    /**
     * Стартовая позиция
     * */
    private BoardPosition start;
    /**
     * Целевая позиция
     * */
    private BoardPosition target;

    public Move(BoardPosition start, BoardPosition target) {
        this.start = start;
        this.target = target;
    }

    public BoardPosition getStart() {
        return start;
    }

    public void setStart(BoardPosition start) {
        this.start = start;
    }

    public BoardPosition getTarget() {
        return target;
    }

    public void setTarget(BoardPosition target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return Objects.equals(start, move.start) && Objects.equals(target, move.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, target);
    }

    @Override
    public String toString() {
        return "Move{" +
                "start=" + start +
                ", target=" + target +
                '}';
    }
}
