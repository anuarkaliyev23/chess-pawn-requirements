package kz.mathncode.anuarkaliyev23.example.chess.ui;

public interface ChessBoardUI {
    void showBoard();
    void showImpossibleMoveWarning();
    void showCheckWarning();
    void showCheckMateWarning();
}
