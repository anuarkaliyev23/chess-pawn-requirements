package kz.mathncode.anuarkaliyev23.example.chess.parser;

import kz.mathncode.anuarkaliyev23.example.chess.game.Move;
import kz.mathncode.anuarkaliyev23.example.chess.game.PawnTransformationMove;
import kz.mathncode.anuarkaliyev23.example.chess.game.SimpleMove;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

public class BasicMoveParser implements MoveParser {
    @Override
    public Move parse(String moveLine) {
        Move move = parseSimpleMove(moveLine);
        return move;
    }

    private SimpleMove parseSimpleMove(String moveLine) {
        String[] strings = moveLine.split("-");
        BoardPosition from = parsePosition(strings[0]);
        BoardPosition target = parsePosition(strings[1]);
        return new SimpleMove(from, target);
    }

    private BoardPosition parsePosition(String position) {
        Vertical vertical = Vertical.valueOf(String.valueOf(position.charAt(0)));
        Horizontal horizontal = Horizontal.valueOf(String.valueOf(String.valueOf(position.charAt(1))));
        return new BoardPosition(vertical, horizontal);
    }
}
