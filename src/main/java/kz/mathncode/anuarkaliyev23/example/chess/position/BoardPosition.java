package kz.mathncode.anuarkaliyev23.example.chess.position;

import java.util.Objects;

/**
 * Класс, необходимый для хранения позиций позиций на доске
 * */
public class BoardPosition {
    /**
     * Вертикаль
     * */
    private Vertical vertical;
    /**
     * Горизонталь
     * */
    private Horizontal horizontal;

    public BoardPosition(Vertical vertical, Horizontal horizontal) {
        this.vertical = vertical;
        this.horizontal = horizontal;
    }

    public BoardPosition(BoardPosition position) {
        this.vertical = position.getVertical();
        this.horizontal = position.getHorizontal();
    }

    public Vertical getVertical() {
        return vertical;
    }

    public void setVertical(Vertical vertical) {
        this.vertical = vertical;
    }

    public Horizontal getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(Horizontal horizontal) {
        this.horizontal = horizontal;
    }

    public int deltaVertical(BoardPosition other) {
        return this.vertical.getIndex() - other.vertical.getIndex();
    }

    public int deltaHorizontal(BoardPosition other) {
        return this.horizontal.getIndex() - other.horizontal.getIndex();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardPosition that = (BoardPosition) o;
        return vertical == that.vertical && horizontal == that.horizontal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vertical, horizontal);
    }

    @Override
    public String toString() {
        return "BoardPosition{" +
                "vertical=" + vertical +
                ", horizontal=" + horizontal +
                '}';
    }
}
