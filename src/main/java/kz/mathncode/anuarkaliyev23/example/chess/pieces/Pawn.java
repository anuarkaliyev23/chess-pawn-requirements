package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.HashSet;
import java.util.Set;

/**
 * Пешка
 * */
public class Pawn extends Piece {
    public Pawn(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public Pawn(Piece other) {
        super(other);
    }



    @Override
    public boolean possibleMove(BoardPosition target) {
        int deltaHorizontal = target.deltaHorizontal(this.getBoardPosition());
        int deltaVertical = target.deltaVertical(this.getBoardPosition());

        if (Math.abs(deltaVertical) > 0)
            return false;

        if (isWhite()) {
            return ((deltaHorizontal == WHITE_DELTA_HORIZONTAL_NOT_MOVED && !isMoved()) || deltaHorizontal == WHITE_DELTA_HORIZONTAL_IS_MOVED);
        } else {
            return ((deltaHorizontal == BLACK_DELTA_HORIZONTAL_NOT_MOVED && !isMoved()) || deltaHorizontal == BLACK_DELTA_HORIZONTAL_IS_MOVED);
        }
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        int deltaHorizontal = target.deltaHorizontal(this.getBoardPosition());
        Set<BoardPosition> trace = new HashSet<>();
        if (!possibleMove(target) && !possibleEat(target))
            throw new ImpossibleMoveException(this, target);

        if (Math.abs(deltaHorizontal) == 1) {
            return trace;
        } else {
            if (deltaHorizontal == WHITE_DELTA_HORIZONTAL_NOT_MOVED) {
                trace.add(new BoardPosition(this.getBoardPosition().getVertical(), getBoardPosition().getHorizontal().next()));
            } else {
                trace.add(new BoardPosition(this.getBoardPosition().getVertical(), getBoardPosition().getHorizontal().prev()));
            }
        }
        return trace;
    }

    @Override
    public boolean possibleEat(BoardPosition target) {
        int deltaHorizontal = target.deltaHorizontal(getBoardPosition());
        int deltaVertical = target.deltaVertical(getBoardPosition());

        if (isWhite()) {
            return (deltaHorizontal == 1 && Math.abs(deltaVertical) == 1);
        } else {
            return (deltaHorizontal == -1 && Math.abs(deltaVertical) == 1);
        }
    }

    @Override
    public String unicodeSymbol() {
        return isWhite() ? "\u2659" : "\u265f";
    }

    private static final int WHITE_DELTA_HORIZONTAL_IS_MOVED = 1;
    private static final int WHITE_DELTA_HORIZONTAL_NOT_MOVED = 2;

    private static final int BLACK_DELTA_HORIZONTAL_IS_MOVED = -1;
    private static final int BLACK_DELTA_HORIZONTAL_NOT_MOVED = -2;
}
