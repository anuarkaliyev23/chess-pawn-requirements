package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

public class NotImplementedException extends ChessException {
    public NotImplementedException() {
    }

    public NotImplementedException(String message) {
        super(message);
    }
}
