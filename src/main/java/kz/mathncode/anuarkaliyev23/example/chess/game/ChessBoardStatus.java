package kz.mathncode.anuarkaliyev23.example.chess.game;

public enum ChessBoardStatus {
    WHITE_CHECK,
    BLACK_CHECK,
    ORDINARY,
    WHITE_MATE,
    BLACK_MATE,
}
