package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.List;

public interface ChessBoard {
    void handle(Move move);
    List<Piece> pieces();
    Piece pieceByPosition(BoardPosition position);
    ChessBoardStatus status();
    List<Move> possibleMoves(Piece p);
}
