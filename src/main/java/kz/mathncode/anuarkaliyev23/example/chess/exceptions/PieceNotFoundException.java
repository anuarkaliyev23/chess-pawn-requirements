package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

public class PieceNotFoundException extends ChessException {
    private BoardPosition position;

    public PieceNotFoundException(BoardPosition position) {
        super(String.format("Piece not found on position={%s}", position));
        this.position = position;
    }

    public BoardPosition getPosition() {
        return position;
    }

    public void setPosition(BoardPosition position) {
        this.position = position;
    }

}
