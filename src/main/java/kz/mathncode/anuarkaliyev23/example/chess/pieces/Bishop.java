package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

import java.util.HashSet;
import java.util.Set;

/**
 * Слон
 * */
public class Bishop extends Piece {
    public Bishop(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public Bishop(Piece other) {
        super(other);
    }

    @Override
    public boolean possibleMove(BoardPosition target) {
        if (getBoardPosition().equals(target))
            return false;

        int deltaVertical = target.deltaVertical(getBoardPosition());
        int deltaHorizontal = target.deltaHorizontal(getBoardPosition());
        return Math.abs(deltaHorizontal) == Math.abs(deltaVertical);
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        int deltaVertical = target.deltaVertical(getBoardPosition());
        int deltaHorizontal = target.deltaHorizontal(getBoardPosition());

        Set<BoardPosition> trace = new HashSet<>();

        if (deltaHorizontal > 0 && deltaVertical > 0) {
//            System.out.printf("deltaHorizontal={%s} deltaVertical={%s}%n", deltaHorizontal, deltaVertical);
            BoardPosition position = new BoardPosition(this.getBoardPosition());
            while (!position.equals(target)) {
                trace.add(position);
//                System.out.printf("Added position={%s} to trace", position);
                position = new BoardPosition(position.getVertical().next(), position.getHorizontal().next());
            }
        } else if (deltaHorizontal > 0 && deltaVertical < 0) {
//            System.out.printf("deltaHorizontal={%s} deltaVertical={%s}%n", deltaHorizontal, deltaVertical);
            BoardPosition position = new BoardPosition(this.getBoardPosition());
            while (!position.equals(target)) {
                trace.add(position);
//                System.out.printf("Added position={%s} to trace", position);
                position = new BoardPosition(position.getVertical().prev(), position.getHorizontal().next());
            }
        } else if (deltaHorizontal < 0 && deltaVertical > 0) {
//            System.out.printf("deltaHorizontal={%s} deltaVertical={%s}%n", deltaHorizontal, deltaVertical);
            BoardPosition position = new BoardPosition(this.getBoardPosition());
            while (!position.equals(target)) {
                trace.add(position);
//                System.out.printf("Added position={%s} to trace", position);
                position = new BoardPosition(position.getVertical().next(), position.getHorizontal().prev());
            }
        } else {
//            System.out.printf("deltaHorizontal={%s} deltaVertical={%s}%n", deltaHorizontal, deltaVertical);
            BoardPosition position = new BoardPosition(this.getBoardPosition());
            while (!position.equals(target)) {
                trace.add(position);
//                System.out.printf("Added position={%s} to trace", position);
                position = new BoardPosition(position.getVertical().prev(), position.getHorizontal().prev());
            }
        }
        trace.remove(getBoardPosition());
        trace.remove(target);
        return trace;
    }

    @Override
    public String unicodeSymbol() {
        if (isWhite()) {
            return "\u2657";
        } else {
            return "\u265d";
        }
    }
}
