package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.Set;

/**
 * Ферзь
 * */
public class Queen extends Piece {
    public Queen(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public Queen(Piece other) {
        super(other);
    }

    @Override
    public boolean possibleMove(BoardPosition target) {
        Rook rook = new Rook(this);
        Bishop bishop = new Bishop(this);
        return rook.possibleMove(target) || bishop.possibleMove(target);
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        if (!possibleMove(target))
            throw new ImpossibleMoveException(this, target);

        Rook rook = new Rook(this);
        Bishop bishop = new Bishop(this);

        if (rook.possibleMove(target)) {
            return rook.trace(target);
        } else {
            return bishop.trace(target);
        }
    }

    @Override
    public String unicodeSymbol() {
        return isWhite() ? "\u2655" : "\u265b";
    }
}
