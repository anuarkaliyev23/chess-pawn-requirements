package kz.mathncode.anuarkaliyev23.example.chess.position;

public enum Horizontal {
    I(1),
    II(2),
    III(3),
    IV(4),
    V(5),
    VI(6),
    VII(7),
    VIII(8);

    /**
     * Индекс, хранящий порядковый номер
     * */
    private final int index;

    Horizontal(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public Horizontal next() {
        if (this == Horizontal.VIII) {
            return I;
        } else {
            return Horizontal.fromIndex(getIndex() + 1);
        }
    }

    public Horizontal prev() {
        if (this == Horizontal.I) {
            return VIII;
        } else {
            return Horizontal.fromIndex(getIndex() - 1);
        }
    }

    public static Horizontal fromIndex(int index) {
        return Horizontal.values()[index - 1];
    }
}
