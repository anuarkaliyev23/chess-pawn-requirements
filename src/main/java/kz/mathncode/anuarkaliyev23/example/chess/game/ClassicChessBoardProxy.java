package kz.mathncode.anuarkaliyev23.example.chess.game;

import kz.mathncode.anuarkaliyev23.example.chess.configuration.Proxy;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ChessException;
import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.List;

public class ClassicChessBoardProxy implements Proxy<ClassicChessBoard>, ChessBoard {
    private ClassicChessBoard classicChessBoard;

    public ClassicChessBoardProxy(ClassicChessBoard classicChessBoard) {
        this.classicChessBoard = classicChessBoard;
    }

    public ClassicChessBoard getClassicChessBoard() {
        return classicChessBoard;
    }

    @Override
    public ClassicChessBoard instance() {
        return classicChessBoard;
    }

    @Override
    public void handle(Move move) {
        try {
            ClassicChessBoard tempBoard = new ClassicChessBoard(classicChessBoard);
            tempBoard.handle(move);

            if (!classicChessBoard.isWhiteMove() && (tempBoard.status() == ChessBoardStatus.BLACK_CHECK && tempBoard.status() == ChessBoardStatus.BLACK_MATE)) {
                throw new ImpossibleMoveException(classicChessBoard.pieceByPosition(move.getStart()), move.getTarget());
            } else if (classicChessBoard.isWhiteMove() && (tempBoard.status() == ChessBoardStatus.WHITE_CHECK && tempBoard.status() == ChessBoardStatus.WHITE_MATE)) {
                throw new ImpossibleMoveException(classicChessBoard.pieceByPosition(move.getStart()), move.getTarget());
            }

            this.classicChessBoard = tempBoard;
        } catch (ChessException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public List<Piece> pieces() {
        return this.classicChessBoard.pieces();
    }

    @Override
    public Piece pieceByPosition(BoardPosition position) {
        return classicChessBoard.pieceByPosition(position);
    }

    @Override
    public ChessBoardStatus status() {
        return classicChessBoard.status();
    }

    @Override
    public List<Move> possibleMoves(Piece p) {
        return classicChessBoard.possibleMoves(p);
    }
}
