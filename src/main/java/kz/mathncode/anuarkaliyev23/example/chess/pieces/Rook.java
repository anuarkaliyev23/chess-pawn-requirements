
package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

import java.util.HashSet;
import java.util.Set;

/**
 * Ладья
 * */
public class Rook extends Piece {
    public Rook(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public Rook(Piece other) {
        super(other);
    }

    @Override
    public boolean possibleMove(BoardPosition target) {
        int deltaVertical = target.deltaVertical(getBoardPosition());
        int deltaHorizontal = target.deltaHorizontal(getBoardPosition());
        if (deltaHorizontal == 0 && deltaVertical == 0)
            return false;

        if (Math.abs(deltaVertical) > 0 && Math.abs(deltaHorizontal) > 0)
            return false;

        return true;
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        if (!possibleMove(target))
            throw new ImpossibleMoveException(this, target);

        Set<BoardPosition> trace = new HashSet<>();
        if (isHorizontalMove(target)) {
            int delta = getBoardPosition().deltaHorizontal(target);
            int start = 0;
            int finish = 0;
            if (delta > 0) {
                start = target.getHorizontal().getIndex();
                finish = getBoardPosition().getHorizontal().getIndex();
            } else {
                start = getBoardPosition().getHorizontal().getIndex();
                finish = target.getHorizontal().getIndex();
            }
            for (int i = start; i < finish ; i++) {
                trace.add(new BoardPosition(getBoardPosition().getVertical(), Horizontal.fromIndex(i)));
            }
        } else {
            int delta = getBoardPosition().deltaVertical(target);
            int start = 0;
            int finish = 0;
            if (delta > 0) {
                start = getBoardPosition().getVertical().getIndex() + 1;
                finish = target.getVertical().getIndex();
            } else {
                start = target.getVertical().getIndex() + 1;
                finish = getBoardPosition().getVertical().getIndex();
            }
            for (int i = start; i < finish ; i++) {
                trace.add(new BoardPosition(Vertical.fromIndex(i), getBoardPosition().getHorizontal()));
            }
        }
        trace.remove(getBoardPosition());
        trace.remove(target);
        return trace;
    }

    @Override
    public String unicodeSymbol() {
        return isWhite() ? "\u2656" : "\u265c";
    }

    private boolean isVerticalMove(BoardPosition target) {
        return Math.abs(getBoardPosition().deltaVertical(target)) > 1;
    }

    private boolean isHorizontalMove(BoardPosition target) {
        return Math.abs(getBoardPosition().deltaHorizontal(target)) > 1;
    }
}
