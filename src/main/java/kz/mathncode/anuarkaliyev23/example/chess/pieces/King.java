package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.exceptions.ImpossibleMoveException;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.HashSet;
import java.util.Set;

public class King extends Piece {
    public King(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public King(Piece other) {
        super(other);
    }

    @Override
    public boolean possibleMove(BoardPosition target) {
        int deltaHorizontal = target.deltaHorizontal(this.getBoardPosition());
        int deltaVertical = target.deltaVertical(this.getBoardPosition());
        return ((Math.abs(deltaVertical) + Math.abs(deltaHorizontal)) <= ACCEPTED_DELTA);
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        if (!possibleMove(target))
            throw new ImpossibleMoveException(this, target);
        else return new HashSet<>();
    }

    @Override
    public String unicodeSymbol() {
        return isWhite() ? "\u2654" : "\u265a";
    }

    public static final int ACCEPTED_DELTA = 3;
}
