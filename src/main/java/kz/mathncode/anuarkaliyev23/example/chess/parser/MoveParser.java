package kz.mathncode.anuarkaliyev23.example.chess.parser;

import kz.mathncode.anuarkaliyev23.example.chess.game.Move;

public interface MoveParser {
    Move parse(String moveLine);
}
