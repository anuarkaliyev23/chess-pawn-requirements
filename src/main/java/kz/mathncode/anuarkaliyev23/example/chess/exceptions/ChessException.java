package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

/**
 * Базовый класс для всех исключений, связанных с шахматами
 * */
public class ChessException extends RuntimeException {
    public ChessException() {
    }

    public ChessException(String message) {
        super(message);
    }

    public ChessException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChessException(Throwable cause) {
        super(cause);
    }
}
