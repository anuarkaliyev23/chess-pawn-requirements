package kz.mathncode.anuarkaliyev23.example.chess.exceptions;

import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

/**
 * Класс исключения, который нужно бросать в случае невозможности хода
 * */
public class ImpossibleMoveException extends ChessException {
    /**
     * Фигура, которой невозможно сделать ход
     * */
    private Piece piece;

    /**
     * Позиция, на которую пытаются походить на фигуру
     * */
    private BoardPosition target;

    public ImpossibleMoveException(Piece piece, BoardPosition target) {
        super(String.format("Piece {%s} cannot move to {%s}",piece, target));
        this.piece = piece;
        this.target = target;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public BoardPosition getTarget() {
        return target;
    }

    public void setTarget(BoardPosition target) {
        this.target = target;
    }
}
