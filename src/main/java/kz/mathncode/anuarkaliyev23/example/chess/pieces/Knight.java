package kz.mathncode.anuarkaliyev23.example.chess.pieces;

import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;

import java.util.HashSet;
import java.util.Set;

/**
 * Конь
 * */
public class Knight extends Piece {
    public Knight(BoardPosition boardPosition, boolean isWhite, boolean isMoved) {
        super(boardPosition, isWhite, isMoved);
    }

    public Knight(Piece other) {
        super(other);
    }

    @Override
    public boolean possibleMove(BoardPosition target) {
        int deltaVertical = target.deltaVertical(getBoardPosition());
        int deltaHorizontal = target.deltaHorizontal(getBoardPosition());
        if (deltaHorizontal == 0 || deltaVertical == 0)
            return false;
        return (Math.abs(deltaHorizontal) + Math.abs(deltaVertical) == ACCEPTED_DELTA);
    }

    @Override
    public Set<BoardPosition> trace(BoardPosition target) {
        return new HashSet<>();
    }

    @Override
    public String unicodeSymbol() {
        return isWhite() ? "\u2658" : "\u265e";
    }

    private static final int ACCEPTED_DELTA = 3;
}
