package kz.mathncode.anuarkaliyev23.example.chess.ui;

import kz.mathncode.anuarkaliyev23.example.chess.game.ChessBoard;
import kz.mathncode.anuarkaliyev23.example.chess.pieces.Piece;
import kz.mathncode.anuarkaliyev23.example.chess.position.BoardPosition;
import kz.mathncode.anuarkaliyev23.example.chess.position.Horizontal;
import kz.mathncode.anuarkaliyev23.example.chess.position.Vertical;

public class ChessBoardConsoleUI implements ChessBoardUI{
    private final ChessBoard board;

    public ChessBoardConsoleUI(ChessBoard board) {
        this.board = board;
    }

    public ChessBoard getBoard() {
        return board;
    }

    @Override
    public void showBoard() {
        for (Horizontal horizontal : Horizontal.values()) {
            for (Vertical vertical : Vertical.values()) {
                System.out.print(CELL_DELIMITER);
                BoardPosition position = new BoardPosition(vertical, horizontal);
                Piece piece = board.pieceByPosition(position);
                if (piece == null) {
                    System.out.print(EMPTY_CELL);
                } else {
                    System.out.print(piece.unicodeSymbol());
                }
            }
            System.out.println(CELL_DELIMITER);
        }
    }

    @Override
    public void showImpossibleMoveWarning() {
        System.out.println("Impossible Move! Try again");
    }

    @Override
    public void showCheckWarning() {
        System.out.println("Check!");
    }

    @Override
    public void showCheckMateWarning() {
        System.out.println("CheckMate!");
    }

    public static final String CELL_DELIMITER = "|";
    public static final String EMPTY_CELL = "\uff3f";
}
